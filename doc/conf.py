import sys
import os.path
import sphinx_rtd_theme

sys.path.append(os.path.abspath('.'))
sys.path.append(os.path.abspath('doc'))

import atimer

extensions = [
    'sphinx.ext.autodoc', 'sphinx.ext.autosummary', 'sphinx.ext.doctest',
    'sphinx.ext.todo', 'sphinx.ext.viewcode'
]
project = 'atimer'
source_suffix = '.rst'
master_doc = 'index'

version = release = atimer.__version__
copyright = 'Artur Wroblewski'

epub_basename = 'atimer - {}'.format(version)
epub_author = 'Artur Wroblewski'

todo_include_todos = True

html_theme = 'sphinx_rtd_theme'

# vim: sw=4:et:ai
